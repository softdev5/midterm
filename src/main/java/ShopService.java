
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mc-so
 */
public class ShopService {

    private static ArrayList<Items> itemList = new ArrayList<Items>();

    static {
        load();
        runID();
    }

    public static void clearList() {
        itemList = new ArrayList<Items>();
        save();
    }

    public static String calculateList() {
        if (!itemList.isEmpty()) {
            double total = 0;
            for (int i = 0; i < itemList.size(); i++) {
                total += itemList.get(i).getTotalPrice();
            }
            return Double.toString(total) + " THB";
        }
        return "0 THB";
    }

    public static String counting() {
        if (!itemList.isEmpty()) {
            int amount = 0;
            for (int i = 0; i < itemList.size(); i++) {
                amount += itemList.get(i).getAmountInt();
            }
            if (itemList.size() == 1) {
                if (itemList.get(0).getAmountInt() == 1) {
                    return Integer.toString(amount) + " Item";
                } else {
                    return Integer.toString(amount) + " Items";
                }
            } else {
                return Integer.toString(amount) + " Items";
            }
        }
        return "0 Item";
    }

    public static String runID() {
        if (!itemList.isEmpty()) {
            int id = 0;
            for (int i = 0; i < itemList.size(); i++) {
                itemList.get(i).setID(i + 1);
                id = i + 2;
            }
            return Integer.toString(id);
        }
        return "1";
    }

    public static boolean addItem(Items item) {
        itemList.add(item);
        save();
        return true;
    }

    public static boolean delItem(Items user) {
        itemList.remove(user);
        save();
        return true;
    }

    public static boolean delItem(int index) {
        itemList.remove(index);
        save();
        return true;
    }

    public static ArrayList<Items> getItems() {
        return itemList;
    }

    public static Items getItem(int index) {
        return itemList.get(index);
    }

    public static boolean updateItem(int index, Items item) {
        itemList.set(index, item);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("sun.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(itemList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ShopService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ShopService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("sun.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            itemList = (ArrayList<Items>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ShopService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ShopService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ShopService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
