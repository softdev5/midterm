
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mc-so
 */
public class Items implements Serializable {

    private int id;
    private String name;
    private String brand;
    private double price;
    private int amount;

    public Items(String name, String brand, double price, int amount) {
        this.id = 0;
        this.brand = brand;
        this.name = name;
        this.price = price;
        this.amount = amount;
    }

    //Getter
    public String getID() {
        String txt = Integer.toString(id);
        return txt;
    }

    public String getBrand() {
        return brand;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        String txt = Double.toString(price);
        return txt;
    }

    public String getAmount() {
        String txt = Integer.toString(amount);
        return txt;
    }

    public int getAmountInt() {
        return amount;
    }

    public Double getTotalPrice() {
        return price * amount;
    }
    
    //Setter
    public void setID(int id) {
        this.id = id;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }
    
    public void setAmount(int anount) {
        this.amount = anount;
    }
    
    @Override
    public String toString() {
        return "ID:" + id + ",    Name:" + name + ",     Brand:" + brand
                + ",    Price:" + price + " THB,    Amount:" + amount;
    }
    
    

}
